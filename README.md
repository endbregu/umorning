### uMorning ###

uMorning is a smart alarm capable of retrieving the user events and automatically set
an alarm to reach the destination. In order to create something useful the application
need to retrieve a lot of information from external source (Facebook, Google maps and
traffic, weather service...).

[github repository](https://github.com/LorenzoCianciaruso/uMorning)